��          D      l       �      �   S   �      �      �   a    $   f  P   �     �  	   �                          The chosen title is unique. There is 1 %2$s with the same title! There are %1$d other %3$s with the same title! post posts Project-Id-Version: Unique Title Checker
POT-Creation-Date: 2016-04-24 15:39+0200
PO-Revision-Date: 
Last-Translator: Bernhard Kau <bernhard@kau-boys.de>
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;esc_html__;esc_html_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: .
X-Textdomain-Support: yes
X-Generator: Poedit 1.8.7
X-Poedit-SearchPath-0: ..
 Der ausgewählte Titel ist einmalig. Es gibt 1 %2$s mit dem gleichen Titel! Es gibt %1$d %3$s mit dem gleichen Titel! Beitrag Beiträge 