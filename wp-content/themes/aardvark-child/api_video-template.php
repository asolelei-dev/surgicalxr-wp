<?php 
/*
Template Name: API Video
*/
get_header('bp'); 
if(!is_user_logged_in()){
	wp_redirect(get_site_url());
}
?>
<link rel="stylesheet" id="wpuf-css-css" href="<?php echo get_site_url();?>/wp-content/plugins/wp-user-frontend/assets/css/frontend-forms.css?ver=5.0.4" type="text/css" media="all">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post();

	// Page options
	$header = ghostpool_option( 'page_header' ) == 'default' ? ghostpool_option( 'page_page_header' ) : ghostpool_option( 'page_header' );
	
?>
	<?php ghostpool_page_title( '', $header ); ?>
	
	<div id="gp-content-wrapper" class="gp-container">
	
		<?php do_action( 'ghostpool_begin_content_wrapper' ); ?>

		<div id="gp-inner-container">

			<div id="gp-content">
				
				<form class="wpuf-form-add wpuf-form-layout1 wpuf-theme-style dv-ajax-form" method="post" action="/addd">

				<ul class="wpuf-form form-label-left">
					<li class="wpuf-el doctor_id">
                    <div class="wpuf-label">
					<label for="post_title_did">Doctor ID <span class="required">*</span></label>
					</div>        
					<div class="wpuf-fields">
					<input class="textfield wpuf_post_title_did" id="post_title_did" type="text" name="doctor_id" placeholder="Please enter video id" value="" size="40" required >
					<span class="wpuf-wordlimit-message wpuf-help"></span>
                    </div>
                    </li>
        
					<li class="wpuf-el video_link">
                    <div class="wpuf-label">
					<label for="video_link">Video Link <span class="required">*</span></label>
					</div>
					<div class="wpuf-fields">
					<input class="textfield video_link" id="video_link" type="url" name="video_link" placeholder="Please enter video link with full path" value="" size="40" required >
					<span class="wpuf-wordlimit-message wpuf-help"></span>
                    </div>
					</li>
        
					<li class="wpuf-submit">
					<div class="wpuf-label">
						&nbsp;
					</div>
					<input type="submit" class="wpuf-submit-button" name="submit" value="Add Video">            
					</li>        
                </ul>
				 <input type="hidden" name="action" value="video_submit" readonly />	
                </form>
				<div class="message" id="bkMess"></div>
			</div>
		</div>
		<?php do_action( 'ghostpool_end_content_wrapper' ); ?>

		<div class="gp-clear"></div>
	</div>

<?php endwhile; endif; ?>
<?php get_footer(); ?>
<Script>
 jQuery(document).ready(function($) {	
	$('.dv-ajax-form').on('submit', function(e){
		e.preventDefault(); 
		var $form = $(this);
		//var FRMid = $form.serializeArray()[0].value;			
		 $('#bkMess').html('Loading...');
		$.post($form.attr('action'), $form.serialize(), function(data) {
		 if(data.error == true){		 
		  $('#bkMess').html(data.error_message);		  
		 } else {
		   var Status = data.status;			 
		  if(Status == "True"){
			$('.dv-ajax-form').hide();
			$('#bkMess').html('Video uploaded successfully!');
		  } else {
			$('#bkMess').html('something wrong!');
		  }
		 }		
		}, 'json');			
	});
 });	
</script>