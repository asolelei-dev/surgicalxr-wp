am4core.ready(function() 
{
	// Themes begin
	am4core.useTheme(am4themes_animated);
	// Themes end

    $.getJSON(homeURL+"/wp-content/themes/aardvark-child/assist/charts/SampleJson.json", function(result){
		drawingChart(result);		
		//loadVideoGallery();
		loadFormList();
    });
}); 
	  
function drawingChart(dataPoints) {
	
	//produce-chart start
	var type_chart = am4core.create("type-chart", am4charts.RadarChart);
	type_chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

	type_chart.data = [
	  {category: "Sunday",value1: 8,value2: 2,value3: 4,value4: 3 },
	  {category: "Monday",value1: 11,value2: 4,value3: 2,value4: 4 },
	  {category: "Tuesday",value1: 7,	value2: 6,value3: 6,value4: 2 },
	  {category: "Wednesday",value1: 13,value2: 8,value3: 3,value4: 2 },
	  {category: "Thursday",value1: 12,value2: 10,	value3: 5,value4: 1},
	  {category: "Friday",value1: 15,	value2: 12,	value3: 4,	value4: 4 },
	  {category: "Saturday",value1: 9,value2: 14,value3: 6,value4: 2 }
	];

	type_chart.padding(20, 20, 20, 20);

	var categoryAxis = type_chart.xAxes.push(new am4charts.CategoryAxis());
	categoryAxis.dataFields.category = "category";
	categoryAxis.renderer.labels.template.location = 0.5;
	categoryAxis.renderer.tooltipLocation = 0.5;

	var valueAxis = type_chart.yAxes.push(new am4charts.ValueAxis());
	valueAxis.tooltip.disabled = true;
	valueAxis.renderer.labels.template.horizontalCenter = "left";
	valueAxis.min = 0;

	var series1 = type_chart.series.push(new am4charts.RadarColumnSeries());
	series1.columns.template.tooltipText = "{name}: {valueY.value}";
	series1.columns.template.width = am4core.percent(80);
	series1.name = "A";
	series1.dataFields.categoryX = "category";
	series1.dataFields.valueY = "value2";
	series1.stacked = true;
	series1.columns.template.fill = am4core.color("#fac732");
	series1.borderAlpha = 0;

	var series2 = type_chart.series.push(new am4charts.RadarColumnSeries());
	series2.columns.template.width = am4core.percent(80);
	series2.columns.template.tooltipText = "{name}: {valueY.value}";
	series2.name = "B";
	series2.dataFields.categoryX = "category";
	series2.dataFields.valueY = "value2";
	series2.stacked = true;
	series2.columns.template.fill = am4core.color("#f4a287");

	var series3 = type_chart.series.push(new am4charts.RadarColumnSeries());
	series3.columns.template.width = am4core.percent(80);
	series3.columns.template.tooltipText = "{name}: {valueY.value}";
	series3.name = "C";
	series3.dataFields.categoryX = "category";
	series3.dataFields.valueY = "value3";
	series3.stacked = true;
	series3.columns.template.fill = am4core.color("#525c6b");
	series3.propertyFields.stroke = "none";
	
	type_chart.seriesContainer.zIndex = -1;
	
	type_chart.cursor = new am4charts.RadarCursor();
	type_chart.cursor.xAxis = categoryAxis;
	type_chart.cursor.fullWidthXLine = true;
	type_chart.cursor.lineX.strokeOpacity = 0;
	type_chart.cursor.lineX.fillOpacity = 0.1;
	type_chart.cursor.lineX.fill = am4core.color("#ffffff");
	
	type_chart.legend = new am4charts.Legend();
	type_chart.legend.position = "top";
/***************************produce-chart end***********************/
/***************************produce-histrogram start****************/	
	var histogram = am4core.create("procedures-histogram", am4charts.XYChart);

	var histrogram_data = [];

	var times1 = dataPoints.process_status_1.tool_time_NAME1TIME;
	var xValues1 = dataPoints.process_status_1.tool_Bounds_NAME1X;
	var yValues1 = dataPoints.process_status_1.tool_Bounds_NAME1Y;

	for (i in times1) {
		var open_value = ((i==0) ? yValues1[0]: yValues1[i-1]);
		//console.log(new Date().setMilliseconds(Number(times1[i])*100));
		histrogram_data.push({
			time: new Date().setMilliseconds(Number(times1[i])*5),
			valueX: xValues1[i], 
			valueY: yValues1[i],
			openValue: open_value,
			columnColor:( yValues1[i] - open_value > 0 ? "#f5b83e":"#545b68")
		});
	}

	histogram.data = histrogram_data;
	// the following line makes value axes to be arranged vertically.
	histogram.leftAxesContainer.layout = "vertical";

	// uncomment this line if you want to change order of axes
	//histogram.bottomAxesContainer.reverseOrder = true;
	var timeAxis = histogram.xAxes.push(new am4charts.DateAxis());
	timeAxis.renderer.grid.template.location = 0;
	timeAxis.renderer.minGridDistance = 50;
	//timeAxis.renderer.ticks.template.length = 0.1;
	timeAxis.renderer.ticks.template.strokeOpacity = 0.1;
	timeAxis.renderer.grid.template.disabled = true;
	timeAxis.renderer.ticks.template.disabled = false;
	timeAxis.renderer.ticks.template.strokeOpacity = 0.2;
	
	timeAxis.baseInterval = {
	  "timeUnit": "millisecond",
	  "count": 1
	};

	// these two lines makes the axis to be initially zoomed-in
	timeAxis.start = 0;
	timeAxis.keepSelection = true;
//////////////////////////////valueY  WaterFallChart//////////////////////////////////////////

	var bounds_nameYAxis = histogram.yAxes.push(new am4charts.ValueAxis());
	bounds_nameYAxis.tooltip.disabled = true;
	bounds_nameYAxis.zIndex = 1;
	bounds_nameYAxis.renderer.baseGrid.disabled = true;
	// height of axis
	bounds_nameYAxis.height = am4core.percent(65);

	bounds_nameYAxis.renderer.gridContainer.background.fill = am4core.color("#ffffff");
	bounds_nameYAxis.renderer.gridContainer.background.fillOpacity = 0.05;
	bounds_nameYAxis.renderer.inside = true;
	bounds_nameYAxis.renderer.labels.template.verticalCenter = "bottom";
	bounds_nameYAxis.renderer.labels.template.padding(2,2,2,2);
	//bounds_nameYAxis.renderer.maxLabelPosition = 0.95;
	bounds_nameYAxis.renderer.fontSize = "0.8em"
	
	var bounds_nameYSeries = histogram.series.push(new am4charts.ColumnSeries());
	bounds_nameYSeries.dataFields.dateX = "time";
	bounds_nameYSeries.dataFields.valueY = "valueY";
	bounds_nameYSeries.dataFields.openValueY = "openValue";
	
	bounds_nameYSeries.tooltipText = "openValue: {openValueY.value}\n value: {valueY.value}";

	var columnTemplate = bounds_nameYSeries.columns.template;
	columnTemplate.strokeOpacity = 0;
	columnTemplate.propertyFields.fill = "columnColor";
	columnTemplate.propertyFields.fillWidth = 100;     

/*	var bounds_nameYAxis = histogram.yAxes.push(new am4charts.ValueAxis());
	bounds_nameYAxis.tooltip.disabled = false;
	// height of axis
	bounds_nameYAxis.height = am4core.percent(65);
	bounds_nameYAxis.zIndex = 1;
	// this makes gap between panels
	bounds_nameYAxis.renderer.baseGrid.disabled = true;
	bounds_nameYAxis.renderer.inside = true;
	bounds_nameYAxis.renderer.labels.template.verticalCenter = "bottom";
	bounds_nameYAxis.renderer.labels.template.padding(2,2,2,2);
	//valueAxis.renderer.maxLabelPosition = 0.95;
	bounds_nameYAxis.renderer.fontSize = "0.8em"

	bounds_nameYAxis.renderer.gridContainer.background.fill = am4core.color("#000000");
	bounds_nameYAxis.renderer.gridContainer.background.fillOpacity = 0.05;

	var bounds_nameYSeries = histogram.series.push(new am4charts.ColumnSeries());
	bounds_nameYSeries.dataFields.dateX = "time";
	bounds_nameYSeries.dataFields.valueY = "valueY";
	bounds_nameYSeries.dataFields.openValueY = "openValue";
	
	bounds_nameYSeries.yAxis = bounds_nameYAxis;
	bounds_nameYSeries.tooltipText = "{valueY.value}";
	bounds_nameYSeries.name = "tool_Bounds_NAME1X";
	
	bounds_nameYSeries.columns.template.fill = am4core.color("#344a7e");
	bounds_nameYSeries.columns.template.fillWidth = 100; 
*/
///////////////////////////////valueX barchart///////////////////////////////////////////
	var bounds_nameXAxis = histogram.yAxes.push(new am4charts.ValueAxis());
	bounds_nameXAxis.tooltip.disabled = false;
	// height of axis
	bounds_nameXAxis.height = am4core.percent(35);
	bounds_nameXAxis.zIndex = 1;
	// this makes gap between panels
	bounds_nameXAxis.marginTop = 30;
	bounds_nameXAxis.renderer.baseGrid.disabled = true;
	bounds_nameXAxis.renderer.inside = true;
	bounds_nameXAxis.renderer.labels.template.verticalCenter = "bottom";
	bounds_nameXAxis.renderer.labels.template.padding(2,2,2,2);
	//valueAxis.renderer.maxLabelPosition = 0.95;
	bounds_nameXAxis.renderer.fontSize = "0.8em"

	bounds_nameXAxis.renderer.gridContainer.background.fill = am4core.color("#ffffff");
	bounds_nameXAxis.renderer.gridContainer.background.fillOpacity = 0.05;

	var bounds_nameXSeries = histogram.series.push(new am4charts.ColumnSeries());
	bounds_nameXSeries.dataFields.dateX = "time";
	bounds_nameXSeries.dataFields.valueY = "valueX";
	bounds_nameXSeries.yAxis = bounds_nameXAxis;
	bounds_nameXSeries.tooltipText = "{valueY.value}";
	bounds_nameXSeries.name = "tool_Bounds_NAME1X";
	
	bounds_nameXSeries.columns.template.fill = am4core.color("#344a7e");
	bounds_nameXSeries.columns.template.fillWidth = 100; 

	histogram.cursor = new am4charts.XYCursor();
	histogram.cursor.xAxis = timeAxis;

	// a separate series for scrollbar
	var scrollbarSeries = histogram.series.push(new am4charts.LineSeries());
	scrollbarSeries.dataFields.dateX = "time";
	scrollbarSeries.dataFields.valueY = "valueX";
	// need to set on default state, as initially series is "show"
	scrollbarSeries.defaultState.properties.visible = false;
	scrollbarSeries.tensionX = 0.8;
	// hide from legend too (in case there is one)
	scrollbarSeries.hiddenInLegend = true;
	scrollbarSeries.fillOpacity = 0.5;
	scrollbarSeries.strokeOpacity = 0.5;

	var scrollbarX = new am4charts.XYChartScrollbar();
	scrollbarX.series.push(scrollbarSeries);
	scrollbarX.marginTop = 20;
	histogram.scrollbarX = scrollbarX;
	histogram.scrollbarX.parent = histogram.bottomAxesContainer;
	//produce-histrogram end
}

/*function loadVideoGallery(){
	for(i=0; i<2; i++)
	{
		console.log(i);
		//$( ".video_gallery" ).append( 
		var $videodiv = $( "<div class='col-md-6'></div>" );
		
		var $imgdiv = $( "<a href='https://apitools.blob.core.windows.net/asset-5846275b-49b8-4677-9db4-e6cfac20b121/Detected Mitral-v2_Cut.mp4?sv=2017-04-17&sr=c&si=7621bd19-8e02-4270-a763-b432ee7e053a&sig=iIZh7vjW1CVaSX0Tqm3bqOA2S4XEwk4A9T4cYGzj1Ik%3D&st=2019-07-03T20%3A44%3A35Z&se=2119-10-01T20%3A44%3A35Z' target='_blank'><img src='"+homeURL+"/wp-content/themes/aardvark-child/assist/charts/VidoeFrame.png' width='100%' height='100%'/></a>" );
		var $titlediv = $( "<h6>Mitral Valve Operation One</h6>" );
		$videodiv.append( $imgdiv );
		$videodiv.append( $titlediv );
		 
		$(".video_gallery").append( $videodiv );
	}
}*/
function loadFormList(){
	for(i=0; i<5; i++)
	{
		$(".form_list").append( "<h6>Mitral Valve Operation One Form 1</h6>" );		
	}
}