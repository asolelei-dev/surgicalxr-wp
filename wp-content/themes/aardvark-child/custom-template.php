<?php 
/*
Template Name: Custom Default
*/
get_header('bp'); 
?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post();

	// Page options
	$header = ghostpool_option( 'page_header' ) == 'default' ? ghostpool_option( 'page_page_header' ) : ghostpool_option( 'page_header' );
	
	?>

	<?php /*ghostpool_page_header( 
		$post_id = get_the_ID(), 
		$type = $header,  
		$bg = ghostpool_option( 'page_header_bg' ),
		$height = ghostpool_option( 'page_header_height', 'height' ) != '' ? ghostpool_option( 'page_header_height', 'height' ) : ghostpool_option( 'page_page_header_height', 'height' )	
	);*/ ?>

	<?php ghostpool_page_title( '', $header ); ?>
	
	
	<?php 
						/* if(is_user_logged_in()){ 
						  $options = array(
									'post_id' => 100000525,
									'field_groups' => array(100000512), 
									//'post_title' => true, 
									'post_content' => false, 							 
									'form' => true,												
									'return' => add_query_arg('updated', 'true', get_permalink(100000525)),
									'html_before_fields' => '<div class="form-patient_consent">',
                                    'html_after_fields' => '</div>',
									'updated_message' => __("Data updated.", 'acf'),
									'html_updated_message' => '<div class="alert alert-success" role="alert">%s</div>',
									//'submit_value' => 'Update' 
								);	 								
					        acf_form($options);
						 }*/ 
						?>
	
	<div id="gp-content-wrapper" class="gp-container">
	
		<?php do_action( 'ghostpool_begin_content_wrapper' ); ?>

		<div id="gp-inner-container">

			<div id="gp-content">
				
				<?php if ( isset( $_GET['post_edit'] ) && $_GET['post_edit'] == 1 ) { 
			
					get_template_part( 'lib/sections/post-submission/post-edit-form' );
		
				} elseif ( isset( $_GET['post_preview'] ) && $_GET['post_preview'] == 1 && isset( $_REQUEST['_wpnonce'] ) && wp_verify_nonce( $_REQUEST['_wpnonce'], 'ghostpool_post_preview_action' ) ) { 
		
					get_template_part( 'lib/sections/post-submission/post-preview' );	
		
				} elseif ( ghostpool_option( 'post_submission_page' ) && is_page( ghostpool_option( 'post_submission_page' ) ) ) {
	
					get_template_part( 'lib/sections/post-submission/post-list' );
				} else {
					get_template_part('lib/sections/page/page-content'); 					
				 if(isset($_GET['id'])){
					 /*Patient-Video*/
				?>
				<script>
				 jQuery(document).ready(function($) {
					$("input[name=patient_id]").val(<?php echo $_GET["id"]?>);
				 });
				</script>
				<?php					 
				 }
				} 
				?>
				
			</div>
	
			<?php get_sidebar( 'left' ); ?>

			<?php get_sidebar( 'right' ); ?>

		</div>

		<?php do_action( 'ghostpool_end_content_wrapper' ); ?>

		<div class="gp-clear"></div>

	</div>

<?php endwhile; endif; ?>

<?php get_footer(); ?>