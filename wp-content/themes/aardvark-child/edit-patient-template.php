<?php 
/*
Template Name: Edit Patient
*/

if(!is_user_logged_in()){
wp_redirect(get_site_url());
 exit;		
}
if(!isset($_GET['pid']) && (!isset($_GET['post_edit']) && $_GET['post_edit'] != 1)){
  wp_redirect(get_site_url());
 exit;	
}
 acf_form_head();
 
$cu_id = get_current_user_id();
$pid = $_GET['pid'];
$auth_id = get_post_field('post_author', $pid);	
if($cu_id != $auth_id){
	wp_redirect(get_site_url());
 exit;	
}

get_header('bp');
$profile_name = get_the_author_meta('user_nicename',$auth_id);	
?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<?php// ghostpool_page_title( '', $header ); ?>
	<link rel='stylesheet' id='acf-input-css'  href='<?php echo get_site_url();?>/wp-content/plugins/advanced-custom-fields-pro/assets/css/acf-input.css?ver=5.7.13' type='text/css' media='all' />
	<div id="buddypress">
<div class="gp-container">
	<div class="item-list-tabs no-ajax" id="object-nav" aria-label="Member primary navigation" role="navigation">
    <div id="gp-bp-tabs-button">Navigation</div>
			<ul style="height: auto;">
					<li id="Dashboard-personal-li"><a id="user-Dashboard" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/Dashboard/">Dashboard</a></li>
				<li id="activity-personal-li" class="">
                    <a id="user-activity" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/activity/">Activity</a></li>
                <li id="xprofile-personal-li"><a id="user-xprofile" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/profile/">Profile</a></li>
                <li id="notifications-personal-li"><a id="user-notifications" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/notifications/">Notifications <span class="no-count"><?php echo bp_notifications_get_unread_notification_count($auth_id);?></span></a></li>
               <li id="messages-personal-li"><a id="user-messages" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/messages/">Messages <span class="no-count"><?php echo bp_total_unread_messages_count($auth_id);?></span></a></li>
                <li id="friends-personal-li"><a id="user-friends" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/friends/">Connections</a></li>
                <li id="groups-personal-li"><a id="user-groups" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/groups/">Groups <span class="no-count"><?php echo bp_total_group_count_for_user($auth_id);?></span></a></li>
                <li id="forums-personal-li"><a id="user-forums" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/forums/">Forums</a></li>
                <li id="mediapress-personal-li"><a id="user-mediapress" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/mediapress/">Gallery <span><?php echo mpp_get_total_gallery_for_user($auth_id)?></span></a></li>	
				<li id="patients-personal-li" class="current selected">
				 <a href="<?php echo get_site_url();?>/operation-case">Patients</a>
				</li>
				<li id="settings-personal-li"><a id="user-settings" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/settings/">Settings</a></li>				
			</ul>
		</div>
    </div>
</div>

	<div id="gp-content-wrapper" class="gp-container single-patientpage">
		
		<?php do_action( 'ghostpool_begin_content_wrapper' ); ?>
		
		<div id="gp-inner-container">
			<div id="gp-content">	

				<article <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">
					
						
						<div class="mainprofilecontent">
							<div class="profletitle">
								<h2 class="gp-loop-title"><?php echo get_the_title();?>	</h2>
							</div>
					
					<div class="gp-entry-content" itemprop="text">
					Patient ID <?php echo get_the_title($pid);?>
						<?php 
						 if(is_user_logged_in()){ 
						  $options = array(
									'post_id' => $pid,
									'field_groups' => array(100000275), 
									//'post_title' => true, 
									'post_content' => true, 							 
									'form' => true,												
									'return' => add_query_arg('updated', 'true', get_permalink($pid)),
									'html_before_fields' => '<div class="form-patient_consent">',
                                    'html_after_fields' => '</div>',
									'updated_message' => __("Data updated.", 'acf'),
									'html_updated_message' => '<div class="alert alert-success" role="alert">%s</div>',
									//'submit_value' => 'Update' 
								);	 								
					        acf_form($options);
						 } 
						?>
						
					</div>
				</div>

				</article>
				
			</div>
		</div>

		<?php do_action( 'ghostpool_end_content_wrapper' ); ?>		
		<div class="gp-clear"></div>
	</div>
<?php endwhile; endif; ?>	
<?php get_footer(); ?>