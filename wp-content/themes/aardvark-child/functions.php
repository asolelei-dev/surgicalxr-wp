<?php

/*
* Add your own functions here. You can also copy some of the theme functions into this file and WordPress will use these functions instead of the original functions.
*/

/**
 * Load parent style.css
 *
 */
if ( ! function_exists( 'ghostpool_enqueue_child_styles' ) ) {
	function ghostpool_enqueue_child_styles() { 
		wp_enqueue_style( 'gp-parent-style', get_template_directory_uri() . '/style.css' );
	}
}
add_action( 'wp_enqueue_scripts', 'ghostpool_enqueue_child_styles' );

/**
 * Load translation file in child theme
 *
 */
if ( ! function_exists( 'ghostpool_child_theme_language' ) ) {
	function ghostpool_child_theme_language() {
		$language_directory = get_stylesheet_directory() . '/languages';
		load_child_theme_textdomain( 'aardvark', $language_directory );
	}
}
add_action( 'after_setup_theme', 'ghostpool_child_theme_language' );

add_action('bp_member_options_nav', 'buddydev_add_profile_extra_custom_links', 10, 0 );
function buddydev_add_profile_extra_custom_links() {	
	 $doctor = '/?doctor='.bp_displayed_user_id();
	if(is_user_logged_in()){
	 if(get_current_user_id() == bp_displayed_user_id()){
	  $doctor = null;	 
	 }		 
	}
	
?>
 <li id="patients-personal-li"><a href="<?php echo get_site_url();?>/operation-case<?php echo $doctor;?>">Patients</a></li>
<?php 
}


function change_content_label($field) {
    $field['label'] = 'Operation report';
    return $field;
}
add_filter('acf/load_field/name=_post_content', 'change_content_label');

function mb_profile_menu_tabs(){
  global $bp;
 $bp->bp_nav['friends']['name'] = 'Connections';
}
add_action('bp_setup_nav', 'mb_profile_menu_tabs', 201);

function dashboardContent() { 
    include_once('inc/charts.php');
}
function dashboardTabScreen(){    
    add_action('bp_template_content', 'dashboardContent');
    bp_core_load_template('buddypress/members/single/plugins');
}

function profile_tab_dasboard(){
      global $bp; 
      bp_core_new_nav_item(array( 
            'name' => 'Dashboard', 
            'slug' => 'Dashboard', 
            'screen_function' => 'dashboardTabScreen', 
            'position' => 1,
            'parent_url'  => bp_loggedin_user_domain().'/dashboard/',
            'parent_slug' => $bp->profile->slug,
            'default_subnav_slug' => 'Dashboard'
      ));
}
add_action('bp_setup_nav', 'profile_tab_dasboard');

function myPmprorhInit() {
	//don't break if Register Helper is not loaded
	if( ! function_exists ( 'pmprorh_add_registration_field' ) ) {
		return false;
	}

	//define the fields
	$fields = array();	
	$fields[0] = new PMProRH_Field(
		'dc_name',
		'text',
		array(
		   'buddypress' => 'Name', 
			'size'      => 40, 
			'label' => 'Name',
			//'class' => '',
			'profile' => false,			
			'required' => true
	));	
	/*$fields[2] = new PMProRH_Field(
		'last_name',
		'text',
		array(
			'label' => 'Last Name',
			'profile' => false,
	));*/	
	$fields[1] = new PMProRH_Field(
		'dc_designation',
		'text',
		array(
			'buddypress' => 'Designation', 
			'label' => 'Designation',
			'profile' => false,			
			'required' => true
	));	
	$fields[2] = new PMProRH_Field (
		'dc_specialization',
		'text',
		array(
			'buddypress' => 'Specialization', 
			'label' => 'Specialization',
			'profile'=> false,
	));
	$fields[3] = new PMProRH_Field (
		'dc_address',
		'text',
		array(
			'buddypress' => 'Address', 
			'label' => 'Address',
			'profile'=> false,
	));
	$fields[4] = new PMProRH_Field (
		'dc_contact',
		'text',
		array(
			'buddypress' => 'Contact', 
			'label' => 'Contact',
			'profile' => false,
	));
	$fields[5] = new PMProRH_Field (
		'dc_hname',
		'text',
		array(
			'buddypress' => 'Hospital Name', 
			'label' => 'Hospital Name',
			'profile' => false,
	));
	$fields[6] = new PMProRH_Field (
		'dc_qualification',
		'text',
		array(
			'buddypress' => 'Qualification', 
			'label' => 'Qualification',
			'profile' => false,
			'required' => true
	));

	//add the fields into a new checkout_boxes are of the checkout page
	foreach( $fields as $field ) {
		pmprorh_add_registration_field(
			'checkout_boxes', // location on checkout page
			$field            // PMProRH_Field object
		);
	}
}
add_action('init', 'myPmprorhInit');

add_action( 'wp_ajax_video_submit', 'video_submit' );
add_action( 'wp_ajax_nopriv_video_submit', 'video_submit' );
function video_submit(){	
	$response = array(
    	'error' => false,
    );	
    $prma = $_POST; 
	//global $wpdb;	
	require_once('inc/api/ApiAzureClass.php');	
	$api_azure = new ApiAzure;		
	$data = array(
			 "doctorID"=> $prma['doctor_id'],
			 "videoResources"=> $prma['video_link']
			);
	$get_data = $api_azure->videoInsert($data);  
	$result = json_decode($get_data);
	
   if(!empty($result)){   
     if(isset($result->status)){
	  $response['status'] = $result->status;           
	 } else {
	  $response['error'] = true;
	  $response['error_message'] = 'something wrong, try again!';    	
	 }
   }
   exit(json_encode($response));	
}
////--- video grid----//
add_action('admin_menu', 'video_grid');

function video_grid()
{
    add_submenu_page( 'edit.php?post_type=video_list', 'Video Grid', 'Video Grid', 'manage_options', 'video_grid', 'video_grid_main');
}

function video_grid_main() 
{ 
  //  $value = get_post_meta( "video_image_list" );
   // echo  get_post_meta(get_the_ID(), 'video_image_list', TRUE);
$args = array(
    'post_type' => 'video_list',
    'posts_per_page' => 20
    // Several more arguments could go here. Last one without a comma.
);

// Query the posts:
$obituary_query = new WP_Query($args);

// Loop through the obituaries:
while ($obituary_query->have_posts()) : $obituary_query->the_post();
    // Echo some markup
   
    // As with regular posts, you can use all normal display functions, such as
 
    // Within the loop, you can access custom fields like so:
    //for image
    $image_video = get_post_meta(get_the_ID(), 'video_image_list', true); 
    $parsed = wp_get_attachment_url( $image_video);

 //for video
     $image_list = get_post_meta(get_the_ID(),'video_embed_list', true);
     $vidls = wp_get_attachment_url( $image_list);
    // Or like so:
  
    echo '<div style="width:30%;padding:8px;display:inline-block" class="wpb_column vc_column_container vc_col-sm-4"> <video width="100%" poster='. $parsed.' controls>
  <source src='. $vidls.' type="video/mp4">
</video></div>'; // Markup closing tags.
endwhile;

// Reset Post Data
wp_reset_postdata();
}
add_filter( 'manage_video_list_posts_columns', 'set_custom_edit_video_columns' );
function set_custom_edit_video_columns($columns){

    $columns['video_link'] = 'Video Link';
    return $columns;
}

add_action( 'manage_video_list_posts_custom_column' , 'custom_video_column', 10, 2 );
function custom_video_column($column, $post_id){
    switch($column){
        case 'video_link' :
          $image_list = get_post_meta($post_id ,'video_embed_list', true);
          $vidls = wp_get_attachment_url($image_list);
            
            if($vidls){
                echo '<a href='.$vidls.' target = "_blank" >View</a>';
            }else{
                echo 'NA';
            }
            break;
    }
}