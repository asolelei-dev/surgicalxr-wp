<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11" />
<?php if ( is_singular() && pings_open( get_queried_object() ) ) { ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php } ?>
<?php bp_head(); ?>
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<?php if ( ghostpool_option( 'page_loader' ) == 'enabled' ) { ?>
	<div id="gp-page-loader">
		<div class="sk-folding-cube">
			<div class="sk-cube1 sk-cube"></div> <div class="sk-cube2 sk-cube"></div> <div class="sk-cube4 sk-cube"></div> <div class="sk-cube3 sk-cube"></div> 
		 </div>
	</div>
<?php } ?>

<?php 

// Page options
if ( function_exists( 'bp_is_active' ) && bp_is_register_page() ) {
	$header_display = ghostpool_option( 'bp_register_header_display' ) != 'default' ? ghostpool_option( 'bp_register_header_display' ) : ghostpool_option( 'header_display' );	
} elseif ( function_exists( 'bp_is_active' ) && bp_is_activation_page() ) {
	$header_display = ghostpool_option( 'bp_activate_header_display' ) != 'default' ? ghostpool_option( 'bp_activate_header_display' ) : ghostpool_option( 'header_display' );	
} else {
	$header_display = get_post_meta( get_the_ID(), 'page_header_display', true ) != 'default' ? get_post_meta( get_the_ID(), 'page_header_display', true ) : ghostpool_option( 'header_display' );	
} ?>

<?php if ( has_nav_menu( 'gp-mobile-primary-nav' ) OR ( has_nav_menu( 'gp-mobile-profile-nav' ) && is_user_logged_in() ) ) { ?>
	
	<div id="gp-close-mobile-nav-button"></div>

	<?php if ( has_nav_menu( 'gp-mobile-primary-nav' ) ) { ?>
		<nav id="gp-mobile-primary-nav" class="gp-mobile-nav">
			<?php wp_nav_menu( array( 'theme_location' => 'gp-mobile-primary-nav', 'sort_column' => 'menu_order', 'container' => 'ul', 'menu_id' => 'gp-mobile-primary-menu', 'fallback_cb' => 'null', 'walker' => new Ghostpool_Custom_Menu ) ); ?>
		</nav>
	<?php } ?>

	<?php if ( has_nav_menu( 'gp-mobile-profile-nav' ) && is_user_logged_in() ) { ?>
		<nav id="gp-mobile-profile-nav" class="gp-mobile-nav">
			<?php wp_nav_menu( array( 'theme_location' => 'gp-mobile-profile-nav', 'sort_column' => 'menu_order', 'container' => 'ul','menu_id' => 'gp-mobile-profile-menu', 'menu_class' => 'menu gp-profile-menu', 'fallback_cb' => 'null', 'walker' => new Ghostpool_Custom_Menu ) ); ?>
		</nav>
	<?php } ?>

	<div id="gp-mobile-nav-bg"></div>

<?php } ?>

<div id="gp-global-wrapper"<?php if ( $header_display != 'gp-header-disabled' ) { ?> class="gp-active-desktop-side-menu"<?php } ?>>
		
	<?php if ( $header_display != 'gp-header-disabled' ) { get_template_part( 'lib/sections/header/side-menu-left' ); } ?>
		
	<div id="gp-site-wrapper">	

		<?php if ( $header_display != 'gp-header-disabled' ) { ?>

			<?php if ( ghostpool_option( 'top_header' ) != 'gp-top-header-disabled' ) { ?>
				<header id="gp-top-header" class="gp-container">
		
					<div class="gp-container">
		
						<?php if ( has_nav_menu( 'gp-top-header-left-nav' ) ) { ?>	
							<nav id="gp-top-nav-left" class="gp-nav">
								<?php wp_nav_menu( array( 'theme_location' => 'gp-top-header-left-nav', 'sort_column' => 'menu_order', 'container' => 'ul', 'menu_id' => 'gp-top-header-left-menu', 'fallback_cb' => 'null', 'walker' => new Ghostpool_Custom_Menu ) ); ?>		
							</nav>
						<?php } ?>
						<?php if ( has_nav_menu( 'gp-top-header-right-nav' ) ) { ?>	
							<nav id="gp-top-nav-right" class="gp-nav">
								<?php wp_nav_menu( array( 'theme_location' => 'gp-top-header-right-nav', 'sort_column' => 'menu_order', 'container' => 'ul', 'menu_id' => 'gp-top-header-right-menu', 'fallback_cb' => 'null', 'walker' => new Ghostpool_Custom_Menu ) ); ?>		
							</nav>
						<?php } ?>
			
					</div>
		
					<div class="gp-clear"></div>
		
				</header>
			<?php } ?>
		
			<?php get_template_part( 'lib/sections/header/mobile-header' ); ?>

			<?php get_template_part( 'lib/sections/header/standard-header' ); ?>
	
			<div class="gp-clear"></div>
	
		<?php } ?>
						
		<div id="gp-page-wrapper">	
<?php		
	if(is_user_logged_in() && !isset($_GET['doctor'])){
		$auth_id = get_current_user_id();
	} if(isset($_GET['doctor']) && !empty($_GET['doctor'])){
		$auth_id = $_GET['doctor'];
	} if(is_single()){
		$auth_id = get_post_field('post_author', get_the_ID());
	}
	
    $bp_ubg_img = null;
    $cover_bgimg_url = bp_attachments_get_attachment('url', array('item_id' => $auth_id));	
	if($cover_bgimg_url){
	  $bp_ubg_img = 'background-image: url('.$cover_bgimg_url.');';
	}
?>	

<div id="gp-buddypress-header" style="<?php echo $bp_ubg_img;?>">
		<header id="gp-page-header" class="">	
			<div id="gp-page-header-inner" style="">
				<div class="gp-container" style="height: 320px;">	
				<div id="item-header-avatar">
					<a href="<?php echo get_site_url().'/members/'.get_the_author_meta('user_nicename',$auth_id);?>">
						<div class="gp-user-online">
						 <div class="bp-tooltip" data-bp-tooltip="<?php bp_last_activity($auth_id);?>"></div>
						</div>		
						<img src="<?php echo esc_url(get_avatar_url($auth_id));?>" class="avatar user-7-avatar avatar-210 photo" width="210" height="210" alt="Profile picture of <?php echo get_the_author_meta('display_name',$auth_id);?>" />	
					</a>
				</div>

			<div id="item-header-content">
				<div class="gp-bp-header-title"><?php echo get_the_author_meta('display_name',$auth_id);?></div>
					<h2 class="gp-bp-header-highlight user-nicename">@<?php echo get_the_author_meta('user_nicename',$auth_id);?></h2>
				<span class="activity"><?php bp_last_activity($auth_id);?></span>
				<?php if(bp_is_active('activity')): ?>
					<div class="gp-bp-header-desc">
						<?php bp_activity_latest_update($auth_id);?>
					</div>
				<?php endif; ?>	
				<div class="gp-bp-header-actions"><?php //do_action('bp_member_header_actions');?></div>	
				<div id="gp-author-social-icons"></div>	
				<div id="template-notices" role="alert" aria-atomic="true">		
				</div>									
			</div>					
					</div>					
			</div>
				<div class="gp-blurred-bg" style="<?php echo $bp_ubg_img;?>"></div>
		</header>
		</div>
		<div id="gp-content-wrapper" class="gp-container">
		<div id="gp-inner-container">
		<div id="gp-content">
		<div id="buddypress">
		<div id="item-nav">	
		</div><!-- #item-nav -->
	</div>
	</div>
	</div>
	<div class="gp-clear"></div>
	</div>