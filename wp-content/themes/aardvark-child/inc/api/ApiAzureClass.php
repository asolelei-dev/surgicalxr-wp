<?php  
class ApiAzure{
	
	public $apiURL = 'https://custmvisionapi.azurewebsites.net'; 
 function runCurl($url,$data){
	$ch = curl_init($url);
	$payload = json_encode($data);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$result = curl_exec($ch);
	curl_close($ch);	
   return $result;
 }
 
 function videoInsert($data){
	$url = $this->apiURL.'/api/processvideo/insert/data';	
	$response= $this->runCurl($url, $data);	
   return $response;
 } 
}  
?>