<?php $child_template_directory = get_site_url().'/wp-content/themes/aardvark-child';?>
  <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?php echo $child_template_directory;?>/assist/charts/bower_components/bootstrap/css/bootstrap.min.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo $child_template_directory;?>/assist/charts/assets/icon/feather/css/feather.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?php echo $child_template_directory;?>/assist/charts/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $child_template_directory;?>/assist/charts/assets/css/jquery.mCustomScrollbar.css">
   
    <!--<script src="<?php echo $child_template_directory;?>/assist/charts/amcharts/dist/script/core.js"></script>
	<script src="<?php echo $child_template_directory;?>/assist/charts/amcharts/dist/script/charts.js"></script>
	<script src="<?php echo $child_template_directory;?>/assist/charts/amcharts/dist/script/themes/animated.js"></script>-->
	<!-- Resources -->
	<script src="https://www.amcharts.com/lib/4/core.js"></script>
	<script src="https://www.amcharts.com/lib/4/charts.js"></script>
	<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

	<!--<script type="text/javascript" src="<?php echo $child_template_directory;?>/assist/charts/bower_components/jquery/js/jquery.min.js"></script>-->	
	
    <script type="text/javascript" src="<?php echo $child_template_directory;?>/assist/charts/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo $child_template_directory;?>/assist/charts/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo $child_template_directory;?>/assist/charts/bower_components/bootstrap/js/bootstrap.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?php echo $child_template_directory;?>/assist/charts/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?php echo $child_template_directory;?>/assist/charts/bower_components/modernizr/js/modernizr.js"></script>
    <!-- Chart js -->
    <!--script type="text/javascript" src="files\bower_components\chart.js\js\Chart.js"></script-->
       
    <script src="<?php echo $child_template_directory;?>/assist/charts/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo $child_template_directory;?>/assist/charts/assets/js/SmoothScroll.js"></script>
    <script src="<?php echo $child_template_directory;?>/assist/charts/assets/js/pcoded.min.js"></script>
    <!-- custom js -->
    <script src="<?php echo $child_template_directory;?>/assist/charts/assets/js/vartical-layout.min.js"></script>
    <script type="text/javascript" src="<?php echo $child_template_directory;?>/assist/charts/assets/pages/dashboard/custom-dashboard.js?v=1"></script>
    <script type="text/javascript" src="<?php echo $child_template_directory;?>/assist/charts/assets/js/script.min.js"></script>	
<script>
 var homeURL = '<?php echo get_site_url();?>';
</script> 
	<!-- Chart code -->
	<script src="<?php echo $child_template_directory;?>/assist/charts/load_data.js"></script>
	
<div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
         <!--   <nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">
                    <div class="navbar-logo">                        
                        <img class="img-fluid" src="<?php echo get_site_url();?>/assist/charts/assets/images/logo.png" alt="Theme-Logo">                        
                    </div>
                    <div class="navbar-container container-fluid">
                        <ul class="nav-left">
                            <li class="header-search">
                                <div class="main-search morphsearch-search">
                                    <div class="input-group">
                                        <span class="input-group-addon search-close"><i class="feather icon-x"></i></span>
                                        <input type="text" class="form-control">
                                        <span class="input-group-addon search-btn"><i class="feather icon-search"></i></span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a href="#!" onclick="javascript:toggleFullScreen()">
                                    <i class="feather icon-maximize full-screen"></i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-right">
                            <li class="header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="feather icon-bell"></i>
                                        <span class="badge bg-c-pink">5</span>
                                    </div>
                                    <ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                        <li>
                                            <h6>Notifications</h6>
                                            <label class="label label-danger">New</label>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="d-flex align-self-center img-radius" src="files\assets\images\avatar-4.jpg" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user">John Doe</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="d-flex align-self-center img-radius" src="files\assets\images\avatar-3.jpg" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user">Joseph William</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="d-flex align-self-center img-radius" src="files\assets\images\avatar-4.jpg" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user">Sara Soudein</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="displayChatbox dropdown-toggle" data-toggle="dropdown">
                                        <i class="feather icon-message-square"></i>
                                        <span class="badge bg-c-green">3</span>
                                    </div>
                                </div>
                            </li>
                            <li class="user-profile header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                        <img src="files\assets\images\avatar-4.jpg" class="img-radius" alt="User-Profile-Image">
                                        <span>John Doe</span>
                                        <i class="feather icon-chevron-down"></i>
                                    </div>
                                    <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                        <li>
                                            <a href="#!">
                                                <i class="feather icon-settings"></i> Settings
                                            </a>
                                        </li>
                                        <li>
                                            <a href="user-profile.htm">
                                                <i class="feather icon-user"></i> Profile
                                            </a>
                                        </li>
                                        <li>
                                            <a href="email-inbox.htm">
                                                <i class="feather icon-mail"></i> My Messages
                                            </a>
                                        </li>
                                        <li>
                                            <a href="auth-lock-screen.htm">
                                                <i class="feather icon-lock"></i> Lock Screen
                                            </a>
                                        </li>
                                        <li>
                                            <a href="auth-normal-sign-in.htm">
                                                <i class="feather icon-log-out"></i> Logout
                                            </a>
                                        </li>
                                    </ul>

                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>-->
            <!-- Sidebar chat start -->
            <div id="sidebar" class="users p-chat-user showChat">
                <div class="had-container">
                    <div class="card card_main p-fixed users-main">
                        <div class="user-box">
                            <div class="chat-inner-header">
                                <div class="back_chatBox">
                                    <div class="right-icon-control">
                                        <input type="text" class="form-control  search-text" placeholder="Search Friend" id="search-friends">
                                        <div class="form-icon">
                                            <i class="icofont icofont-search"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="main-friend-list">
                                <div class="media userlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left" title="Josephin Doe">
                                    <a class="media-left" href="#!">
                                        <img class="media-object img-radius img-radius" src="<?php echo $child_template_directory;?>/assist/charts/assets\images\avatar-3.jpg" alt="Generic placeholder image ">
                                        <div class="live-status bg-success"></div>
                                    </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Josephin Doe</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="2" data-status="online" data-username="Lary Doe" data-toggle="tooltip" data-placement="left" title="Lary Doe">
                                    <a class="media-left" href="#!">
                                        <img class="media-object img-radius" src="<?php echo $child_template_directory;?>/assist/charts/assets/images/avatar-2.jpg" alt="Generic placeholder image">
                                        <div class="live-status bg-success"></div>
                                    </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Lary Doe</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="3" data-status="online" data-username="Alice" data-toggle="tooltip" data-placement="left" title="Alice">
                                    <a class="media-left" href="#!">
                                        <img class="media-object img-radius" src="<?php echo $child_template_directory;?>/assist/charts/assets/images/avatar-4.jpg" alt="Generic placeholder image">
                                        <div class="live-status bg-success"></div>
                                    </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Alice</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="4" data-status="online" data-username="Alia" data-toggle="tooltip" data-placement="left" title="Alia">
                                    <a class="media-left" href="#!">
                                        <img class="media-object img-radius" src="<?php echo $child_template_directory;?>/assist/charts/assets/images/avatar-3.jpg" alt="Generic placeholder image">
                                        <div class="live-status bg-success"></div>
                                    </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Alia</div>
                                    </div>
                                </div>
                                <div class="media userlist-box" data-id="5" data-status="online" data-username="Suzen" data-toggle="tooltip" data-placement="left" title="Suzen">
                                    <a class="media-left" href="#!">
                                        <img class="media-object img-radius" src="<?php echo $child_template_directory;?>/assist/charts/assets/images/avatar-2.jpg" alt="Generic placeholder image">
                                        <div class="live-status bg-success"></div>
                                    </a>
                                    <div class="media-body">
                                        <div class="f-13 chat-header">Suzen</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Sidebar inner chat start-->
            <div class="showChat_inner">
                <div class="media chat-inner-header">
                    <a class="back_chatBox">
                        <i class="feather icon-chevron-left"></i> Josephin Doe
                    </a>
                </div>
                <div class="media chat-messages">
                    <a class="media-left photo-table" href="#!">
                        <img class="media-object img-radius img-radius m-t-5" src="<?php echo $child_template_directory;?>/assist/charts/assets/images/avatar-3.jpg" alt="Generic placeholder image">
                    </a>
                    <div class="media-body chat-menu-content">
                        <div class="">
                            <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                            <p class="chat-time">8:20 a.m.</p>
                        </div>
                    </div>
                </div>
                <div class="media chat-messages">
                    <div class="media-body chat-menu-reply">
                        <div class="">
                            <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                            <p class="chat-time">8:20 a.m.</p>
                        </div>
                    </div>
                    <div class="media-right photo-table">
                        <a href="#!">
                            <img class="media-object img-radius img-radius m-t-5" src="<?php echo $child_template_directory;?>/assist/charts/assets/images/avatar-4.jpg" alt="Generic placeholder image">
                        </a>
                    </div>
                </div>
                <div class="chat-reply-box p-b-20">
                    <div class="right-icon-control">
                        <input type="text" class="form-control search-text" placeholder="Share Your Thoughts">
                        <div class="form-icon">
                            <i class="feather icon-navigation"></i>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Sidebar inner chat end-->
            <div class="pcoded-main-container">                                
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <!--  Cardiothorocic start -->
                                    <div class="col-xl-8 col-md-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h3>Cardiothoracic Procedures</h3>                                                        
                                                <div class="card-header-right">
                                                    <ul class="list-unstyled card-option">
                                                        <li><i class="feather icon-maximize full-card"></i></li>
                                                        <li><i class="feather icon-minus minimize-card"></i></li>
                                                        <li><i class="feather icon-trash-2 close-card"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="card-block">                                                
                                                <div id="procedures-histogram" style="height: 500px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--  Cardiothorocic end -->
                                    <div class="col-xl-4 col-md-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h3>Procedure Types</h3>                                                        
                                                <div class="card-header-right">
                                                    <ul class="list-unstyled card-option">
                                                        <li><i class="feather icon-maximize full-card"></i></li>
                                                        <li><i class="feather icon-minus minimize-card"></i></li>
                                                        <li><i class="feather icon-trash-2 close-card"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <div id="type-chart" style="height: 500px;"></div>
                                            </div>
                                        </div>
                                    </div>
									<div class="col-xl-8 col-md-12">
										<div class="card table-card">
											<div class="card-header">
												<h5>Video Gallery</h5>
												<div class="card-header-right">
													<ul class="list-unstyled card-option">
														<li><i class="feather icon-maximize full-card"></i></li>
														<li><i class="feather icon-minus minimize-card"></i></li>
														<li><i class="feather icon-trash-2 close-card"></i></li>
													</ul>
												</div>
											</div>
											<div class="card-block">
												<div class="table-responsive">
													<div class="col">
														<div class="video_gallery_new row" style="margin:10px;text-align:center;">	
														
	
<div class="col-md-6"><a href="https://apitools.blob.core.windows.net/asset-3f507911-3c21-4918-b494-8ca9b30ef1b9/DetectedMitralCut.webm?sv=2017-04-17&sr=c&si=d53101b7-90ef-4181-b15b-7b26087e8ff2&sig=siqoYjiLdXfAN7GsE7sb%2BA9nccZSbLhyezejicZHpAQ%3D&st=2019-07-11T00%3A47%3A42Z&se=2119-07-11T00%3A47%3A42Z
" target="_blank"><img src="https://www.surgicalxr.com/wp-content/themes/aardvark-child/assist/charts/VidoeFrame.png" width="100%" height="100%"></a><h6>Robo Mitral-20190213</h6></div>

<div class="col-md-6"><a href="https://apitools.blob.core.windows.net/asset-3f507911-3c21-4918-b494-8ca9b30ef1b9/DetectedMitralCut.webm?sv=2017-04-17&sr=c&si=d53101b7-90ef-4181-b15b-7b26087e8ff2&sig=siqoYjiLdXfAN7GsE7sb%2BA9nccZSbLhyezejicZHpAQ%3D&st=2019-07-11T00%3A47%3A42Z&se=2119-07-11T00%3A47%3A42Z
" target="_blank"><img src="https://www.surgicalxr.com/wp-content/themes/aardvark-child/assist/charts/VidoeFrame.png" width="100%" height="100%"></a><h6>Robo Mitral-20190530</h6></div>

<div class="col-md-6"><a href="https://apitools.blob.core.windows.net/asset-22e905e3-c28f-4971-a29f-99db63d2dfd6/Detected_LIMA_LAD_MKW.webm?sv=2017-04-17&sr=c&si=3ab4384d-6383-4566-bfb0-e94994930a49&sig=%2B98FY6sPK7mONb990w99wpOjsANDtf5GGL4%2FkgB%2Bpxk%3D&st=2019-07-15T17%3A16%3A33Z&se=2119-07-15T17%3A16%3A33Z
" target="_blank"><img src="https://www.surgicalxr.com/wp-content/themes/aardvark-child/assist/charts/screenshot.jpg" width="100%" height="100%"></a><h6>Robo Mitral-20190626</h6></div>

														
														</div>
													</div>
													<div class="text-center">
														<a href="#!" class="b-b-primary text-primary">View all Videos</a>
													</div>
												</div>
											</div>
										</div>
                                    </div>
									<div class="col-xl-4 col-md-12">
										<div class="card user-activity-card">
											<div class="card-header">
												<h5>Patient and Doctor Forms</h5>
											</div>
											<div class="card-block">
												<div class="row" style="padding:15px;">
													<div class="form_list"></div>
												</div>
												<div class="text-center">
													<a href="#!" class="b-b-primary text-primary">View all Forms</a>
												</div>
											</div>
										</div>
                                    </div>									
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>	