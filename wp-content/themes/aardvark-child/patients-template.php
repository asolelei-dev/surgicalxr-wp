<?php
/*
Template Name: Patients
*/
get_header('bp');
//global $bp;
?>
<?php		
	if(is_user_logged_in() && !isset($_GET['doctor'])){
		$auth_id = get_current_user_id();
	} if(isset($_GET['doctor']) && !empty($_GET['doctor'])){
		$auth_id = $_GET['doctor'];
	}
    $profile_name = get_the_author_meta('user_nicename',$auth_id);	
?>	
<?php
// Page options
$header = ghostpool_option( 'page_header' ) == 'default' ? ghostpool_option( 'page_page_header' ) : ghostpool_option( 'page_header' );
$height = ghostpool_option( 'page_header_height', 'padding-bottom' ) != '' ? ghostpool_option( 'page_header_height', 'padding-bottom' ) : ghostpool_option( 'page_page_header_height', 'height' );
$format = ghostpool_option( 'blog_format' );
$style = ghostpool_option( 'blog_style' );
$alignment = ghostpool_option( 'blog_alignment' );
$cats = ghostpool_option( 'blog_cats' ) ? implode( ',', ghostpool_option( 'blog_cats' ) ) : '';
$post_types = ghostpool_option( 'blog_post_types' ) ? implode( ',', ghostpool_option( 'blog_post_types' ) ) : ghostpool_option( 'blog_post_types' );
$orderby = ghostpool_option( 'blog_orderby' );
$per_page = ghostpool_option( 'blog_per_page' );
$offset = ghostpool_option( 'blog_offset' );
$image_size = ghostpool_option( 'blog_image_size' );
$content_display = ghostpool_option( 'blog_content_display' );	
$excerpt_length = ghostpool_option( 'blog_excerpt_length' );
$meta_author = ghostpool_option( 'blog_meta', 'author' );
$meta_date = ghostpool_option( 'blog_meta', 'date' );
$meta_comment_count = ghostpool_option( 'blog_meta', 'comment_count' );
$meta_views = ghostpool_option( 'blog_meta', 'views' );
$meta_likes = ghostpool_option( 'blog_meta', 'likes' );
$meta_cats = ghostpool_option( 'blog_meta', 'cats' );
$meta_tags = ghostpool_option( 'blog_meta', 'tags' );
$read_more_link = ghostpool_option( 'blog_read_more_link' );
$pagination = 'page-numbers'; 
 if(!is_user_logged_in() && !isset($_GET['doctor'])){
	 wp_redirect(get_site_url());
	exit;
 }
 //wp_nav_menu('Buddypress');
?>
<div id="buddypress">
<div class="gp-container">
	<div class="item-list-tabs no-ajax" id="object-nav" aria-label="Member primary navigation" role="navigation">
    <div id="gp-bp-tabs-button">Navigation</div>
			<ul style="height: auto;">
				<li id="Dashboard-personal-li"><a id="user-Dashboard" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/Dashboard/">Dashboard</a></li>
				<li id="activity-personal-li">
                    <a id="user-activity" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/activity/">Activity</a></li>
                <li id="xprofile-personal-li"><a id="user-xprofile" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/profile/">Profile</a></li>
				
			<?php if(!isset($_GET['doctor'])){?>	
                <li id="notifications-personal-li"><a id="user-notifications" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/notifications/">Notifications <span class="no-count">
				<?php echo bp_notifications_get_unread_notification_count($auth_id);?></span></a></li>
                <li id="messages-personal-li"><a id="user-messages" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/messages/">Messages <span class="no-count"><?php echo bp_total_unread_messages_count($auth_id);?></span></a></li>
			<?php } ?>
			
                <li id="friends-personal-li"><a id="user-friends" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/friends/">Connections <?php //<span class="no-count"><?php echo bp_total_friend_count($auth_id);</span>?></a></li>
                <li id="groups-personal-li"><a id="user-groups" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/groups/">Groups <span class="no-count"><?php echo bp_total_group_count_for_user($auth_id);?></span></a></li>
                <li id="forums-personal-li"><a id="user-forums" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/forums/">Forums</a></li>
			<?php if(function_exists('mpp_get_total_gallery_for_user')){?>
                <li id="mediapress-personal-li"><a id="user-mediapress" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/mediapress/">Gallery <span><?php echo mpp_get_total_gallery_for_user($auth_id)?></span></a></li>
			<?php } ?>				
				<li id="patients-personal-li" class="current selected">
				<?php if(isset($_GET['doctor'])){?>
				 <a href="<?php echo get_site_url();?>/operation-case/?doctor=<?php echo $_GET['doctor'];?>">Patients</a>
				<?php } else { ?>
				 <a href="<?php echo get_site_url();?>/operation-case">Patients</a>
				<?php } ?>
				</li>	
				<?php if(!isset($_GET['doctor'])){?>
				<li id="settings-personal-li"><a id="user-settings" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/settings/">Settings</a></li>
				<?php } ?>	
			</ul>
		</div>
    </div>
</div>
	
<div id="gp-content-wrapper" class="gp-container">	

	<?php do_action( 'ghostpool_begin_content_wrapper' ); ?>
	
	<div id="gp-inner-container">

		<div id="gp-content">
<?php if(!isset($_GET['doctor'])){?>
    <div class="cutompagetitle">
	<?php ghostpool_page_title( get_the_ID(), $header ); ?>
	<a href="<?php echo get_permalink();?>add-patient/"><i class="fa fa-cog" aria-hidden="true"></i> Option</a>
	<a href="<?php echo get_permalink();?>add-patient/"><i class="fa fa-plus-circle" aria-hidden="true"></i> Create New</a>	
    </div>
<?php } ?>

	
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
									
				<?php the_content(); ?>
	
			<?php endwhile; endif; rewind_posts(); ?>	

			<?php 
			$args = array(			 
				'author'          => $auth_id,
				'post_status' 	  => 'publish',
				'post_type'       => 'patients',
				'orderby'         => 'ID',
				'order'           => 'DESC',
				'posts_per_page'  => 10,
				'paged'           => ghostpool_paged(),
			);

			$args = apply_filters( 'ghostpool_blog_query', $args );

			$gp_query = new WP_Query( $args );

			// Classes
			$css_classes = array(
				'gp-posts-wrapper',
				'gp-archive-wrapper',
				$format,
				$style,
				$alignment,
			);
			$css_classes = trim( implode( ' ', array_filter( array_unique( $css_classes ) ) ) );
			?>
		
			<div class="<?php echo esc_attr( $css_classes ); ?>" data-type="blog-template"<?php if ( function_exists( 'ghostpool_filter_variables' ) ) { echo ghostpool_filter_variables( '', $cats, $post_types, $format, $style, $orderby, $per_page, $offset, $image_size, $content_display, $excerpt_length, $meta_author, $meta_date, $meta_comment_count, $meta_views, $meta_likes, $meta_cats, $meta_tags, $read_more_link, $pagination ); } ?>>
			
					<?php ghostpool_filter( get_post_meta( get_the_ID(), 'blog_filters', true ), get_post_meta( get_the_ID(), 'blog_filter_cat_id', true ), $orderby, 'page-numbers' ); ?>
													
					<div class="gp-section-loop <?php echo sanitize_html_class( ghostpool_option( 'ajax' ) ); ?>">
						
						<?php if ( $gp_query->have_posts() ) : ?>
						
							<div class="gp-section-loop-inner clearfix">								
								<?php if ( $format == 'gp-posts-masonry' ) { ?><div class="gp-gutter-size"></div><?php } ?>					
								<?php while ( $gp_query->have_posts() ) : $gp_query->the_post(); ?>
								<?php 
								 $pid  = get_the_ID(); 
								 $fname= get_field('patient_first_name',$pid);
								 $mname= get_field('patient_middle_name',$pid);
								 $lname= get_field('patient_last_name',$pid);
								 $date_procedure= get_field('patient_date_procedure',$pid);
								 if($date_procedure){
									$date_procedure = date('d M Y', strtotime($date_procedure));
								 }
								 $surgeon= get_field('patient_surgeon',$pid);
								 $gender= get_field('patient_gender',$pid);
								 $patient_img_url= get_site_url().'/wp-content/uploads/2019/05/user-female.png';
								if($gender == 'male'){
								 $patient_img_url= get_site_url().'/wp-content/uploads/2019/05/user_male.png';
								}									
								
								 $patient_name = $fname;
								 if($mname){
								   $patient_name .= ' '.$mname;
								 }
								 if($lname){
								   $patient_name .= ' '.$lname;
								 }
								?>
								
					<section class="oprationcase-section gp-post-item post-<?php echo $pid;?> patients type-patients status-publish has-post-thumbnail pmpro-has-access">		
					<div class="coverphoto">	
					
					</div>				
					<div class="gp-loop-content">
						<div class="gp-post-thumbnail gp-loop-featured">
					 <?php if($patient_img_url){?>					
					  <img src="<?php echo $patient_img_url;?>" class="attachment-gp_list_image size-gp_list_image wp-post-image" alt="" width="82" height="82">			
					 <?php }?>							
					</div>	
						<h2 class="gp-loop-title">
						 <a href="<?php echo get_permalink();?>" title="<?php echo $pid;?>"><?php echo $patient_name.' '.get_the_title();?></a>
						</h2>
						<div class="gp-loop-meta">
						 <span class="gp-post-meta gp-meta-date"><?php echo $date_procedure;?></span>
						</div>
						<div class="gp-loop-text">
							<?php the_content();?>
						</div>				
						<div class="gp-loop-meta">
						  <span class="gp-post-meta gp-meta-author">							
							<a href="<?php echo get_site_url().'/members/'.get_the_author_meta('user_nicename');?>">
							<?php echo get_the_author_meta('display_name');?>
							</a>
						  </span>
						</div>
			</div>	
		</section>
								<?php endwhile; ?>
							</div>
						
					<?php echo ghostpool_pagination( $gp_query->max_num_pages, 'page-numbers' ); ?>
					
					<?php else : ?>

						<strong class="gp-no-items-found"><?php esc_html_e( 'No record found.', 'aardvark' ); ?></strong>

					<?php endif; wp_reset_postdata(); ?>
					
				</div>	
		
			</div>

		</div>
		<?php get_sidebar( 'left' ); ?>	
		<?php get_sidebar( 'right' ); ?>
	</div>

	<?php do_action( 'ghostpool_end_content_wrapper' ); ?>
			
	<div class="gp-clear"></div>

</div>

<?php get_footer(); ?>