<?php get_header('bp'); ?>
<?php
	$pid = get_the_ID();
	if(is_single()){
		$auth_id = get_post_field('post_author', $pid);
	}
	$cu_id = get_current_user_id();	
	$profile_name = get_the_author_meta('user_nicename',$auth_id);	
?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<link rel='stylesheet' id='acf-input-css' href='<?php echo get_site_url();?>/wp-content/plugins/advanced-custom-fields-pro/assets/css/acf-input.css?ver=5.7.13' type='text/css' media='all' />
	<div id="buddypress">
<div class="gp-container">
	<div class="item-list-tabs no-ajax" id="object-nav" aria-label="Member primary navigation" role="navigation">
    <div id="gp-bp-tabs-button">Navigation</div>
			<ul style="height: auto;">
					<li id="Dashboard-personal-li"><a id="user-Dashboard" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/Dashboard/">Dashboard</a></li>
				<li id="activity-personal-li" class="">
                    <a id="user-activity" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/activity/">Activity</a></li>
                <li id="xprofile-personal-li"><a id="user-xprofile" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/profile/">Profile</a></li>
             <?php if($cu_id == $auth_id){?>	
                <li id="notifications-personal-li"><a id="user-notifications" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/notifications/">Notifications <span class="no-count"><?php echo bp_notifications_get_unread_notification_count($auth_id);?></span></a></li>			
                <li id="messages-personal-li"><a id="user-messages" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/messages/">Messages <span class="no-count"><?php echo bp_total_unread_messages_count($auth_id);?></span></a></li>
			<?php } ?>	
                <li id="friends-personal-li"><a id="user-friends" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/friends/">Connections</a></li>
                <li id="groups-personal-li"><a id="user-groups" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/groups/">Groups <span class="no-count"><?php echo bp_total_group_count_for_user($auth_id);?></span></a></li>
                <li id="forums-personal-li"><a id="user-forums" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/forums/">Forums</a></li>
            <?php if(function_exists('mpp_get_total_gallery_for_user')){?>
                <li id="mediapress-personal-li"><a id="user-mediapress" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/mediapress/">Gallery <span><?php echo mpp_get_total_gallery_for_user($auth_id)?></span></a></li>
			<?php } ?>
				<li id="patients-personal-li" class="current selected">
				<?php if($cu_id != $auth_id){?>	
				 <a href="<?php echo get_site_url();?>/operation-case/?doctor=<?php echo $auth_id;?>">Patients</a>
				<?php } else { ?>
				 <a href="<?php echo get_site_url();?>/operation-case">Patients</a>
				<?php } ?>
				</li>	   
			<?php if($cu_id == $auth_id){?>		
				<li id="settings-personal-li"><a id="user-settings" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/settings/">Settings</a></li>	
			<?php } ?>					
			</ul>
		</div>
    </div>
</div>

	<div id="gp-content-wrapper" class="gp-container single-patientpage">		
		<?php do_action( 'ghostpool_begin_content_wrapper' ); ?>		
		<div id="gp-inner-container">
			<div id="gp-content">			
				<?php
				// Page options
				$display_image = ghostpool_option( 'image' ) == 'default' ? ghostpool_option( 'post_image' ) : ghostpool_option( 'image' );
				
				$patient_id= get_field('patient_id',$pid);
				$video_url= get_field('patient_video_url',$pid);
				$lastmodified = get_the_modified_time('U');
			    $posted = get_the_time('U');			
			    $terms = get_the_terms($pid, 'video_category');
			    $cats = null;     
			foreach($terms as $term){
				$cats .= $term->name.', ';
			}
			?>

				<article <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">					
						<?php if ( has_post_thumbnail() && $display_image == 'enabled'){ ?>
							<div class="gp-post-thumbnail gp-entry-featured">
								<?php the_post_thumbnail( 'gp_featured_image' ); ?>
								<?php $attachment_id = get_post( get_post_thumbnail_id() ); if ( $attachment_id->post_excerpt ) { ?><div class="wp-caption-text"><?php echo esc_attr( $attachment_id->post_excerpt ); ?></div><?php } ?>
							</div>
						<?php } ?>
						<div class="mainprofilecontent">
						<div class="profletitle">
							<h2 class="gp-loop-title"><?php echo get_the_title();?></h2>
						<div class="gp-loop-meta">
					<p class="vid-meta"> Posted <?php echo human_time_diff($posted,current_time('U'));?> ago by <a href="<?php echo get_site_url().'/members/'.get_the_author_meta('user_nicename');?>"><?php echo get_the_author_meta('display_name');?></a>
						<?php if(is_user_logged_in() && (get_current_user_id() == $auth_id)){?> 
						 <a href="<?php echo get_site_url();?>/operation-case/edit-patient-video/?pid=<?php echo $pid;?>"><i class="fa fa-cog" aria-hidden="true"></i> Edit</a>
						<?php } ?>	
					</p>						
						</div>
						</div>
					<div class="gp-entry-content" itemprop="text">
					<div class="mpp-item-content mpp-video-content mpp-video-player">
						<iframe width="100%" height="200" src="<?php echo $video_url;?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
					</div>
						<?php the_content(); ?>							
					</div>
						<hr>					
					<div class="mpp-media-meta-bottom">
						<span class="cate"><i class="fa fa-folder"></i> <?php echo rtrim($cats,", ");?></span>
						<span class="view"><i class="fa fa-eye"></i> <?php echo spvc_get_viewcount($pid);?> View</span>
					</div>
				</div>	
				</article>				
			</div>
		</div>
		<?php do_action( 'ghostpool_end_content_wrapper' ); ?>		
		<div class="gp-clear"></div>
	</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>