<?php 
if(is_user_logged_in()) { 
    acf_form_head();
}
get_header('bp');
?>
<?php
	$pid = get_the_ID();
	if(is_single()){
		$auth_id = get_post_field('post_author', $pid);
	}	
    $cu_id = get_current_user_id();	
	$profile_name = get_the_author_meta('user_nicename',$auth_id);	
?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post();

	// Page options
	/*$header = ghostpool_option( 'page_header' ) == 'default' ? ghostpool_option( 'post_page_header' ) : ghostpool_option( 'page_header' );
	
	?>

	<?php ghostpool_page_header( 
		$post_id = get_the_ID(), 
		$type = $header,  
		$bg = ghostpool_option( 'page_header_bg' ),
		$height = ghostpool_option( 'page_header_height', 'height' ) != '' ? ghostpool_option( 'page_header_height', 'height' ) : ghostpool_option( 'post_page_header_height', 'height' )	
	);*/ ?>

	<?php// ghostpool_page_title( '', $header ); ?>
	<link rel='stylesheet' id='acf-input-css' href='<?php echo get_site_url();?>/wp-content/plugins/advanced-custom-fields-pro/assets/css/acf-input.css?ver=5.7.13' type='text/css' media='all' />
	<div id="buddypress">
<div class="gp-container">
	<div class="item-list-tabs no-ajax" id="object-nav" aria-label="Member primary navigation" role="navigation">
    <div id="gp-bp-tabs-button">Navigation</div>
			<ul style="height: auto;">
					<li id="Dashboard-personal-li"><a id="user-Dashboard" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/Dashboard/">Dashboard</a></li>
				<li id="activity-personal-li" class="">
                    <a id="user-activity" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/activity/">Activity</a></li>
                <li id="xprofile-personal-li"><a id="user-xprofile" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/profile/">Profile</a></li>
			<?php if($cu_id == $auth_id){?>	
                <li id="notifications-personal-li"><a id="user-notifications" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/notifications/">Notifications <span class="no-count"><?php echo bp_notifications_get_unread_notification_count($auth_id);?></span></a></li>			
                <li id="messages-personal-li"><a id="user-messages" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/messages/">Messages <span class="no-count"><?php echo bp_total_unread_messages_count($auth_id);?></span></a></li>
			<?php } ?>		
                <li id="friends-personal-li"><a id="user-friends" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/friends/">Connections </a></li>
                <li id="groups-personal-li"><a id="user-groups" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/groups/">Groups <span class="no-count"><?php echo bp_total_group_count_for_user($auth_id);?></span></a></li>
                <li id="forums-personal-li"><a id="user-forums" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/forums/">Forums</a></li>
            <?php if(function_exists('mpp_get_total_gallery_for_user')){?>
                <li id="mediapress-personal-li"><a id="user-mediapress" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/mediapress/">Gallery <span><?php echo mpp_get_total_gallery_for_user($auth_id)?></span></a></li>
			<?php } ?>
				<li id="patients-personal-li" class="current selected">
				<?php if($cu_id != $auth_id){?>	
				 <a href="<?php echo get_site_url();?>/operation-case/?doctor=<?php echo $auth_id;?>">Patients</a>
				<?php } else { ?>
				 <a href="<?php echo get_site_url();?>/operation-case">Patients</a>
				<?php } ?>
				</li>	
			<?php if($cu_id == $auth_id){?>		
				<li id="settings-personal-li"><a id="user-settings" href="<?php echo get_site_url();?>/members/<?php echo $profile_name;?>/settings/">Settings</a></li>	
			<?php } ?>				
			</ul>
		</div>
    </div>
</div>

	<div id="gp-content-wrapper" class="gp-container single-patientpage">		
		<?php do_action( 'ghostpool_begin_content_wrapper' ); ?>
		
		<div id="gp-inner-container">
			<div id="gp-content">			
				<?php

				// Page options
				$display_image = ghostpool_option( 'image' ) == 'default' ? ghostpool_option( 'post_image' ) : ghostpool_option( 'image' );
				
					$fname= get_field('patient_first_name',$pid);
					$mname= get_field('patient_middle_name',$pid);
					$lname= get_field('patient_last_name',$pid);
					$date_procedure= get_field('patient_date_procedure',$pid);
				  if($date_procedure){
					$date_procedure = date('d M Y', strtotime($date_procedure));
				  }
					$surgeon= get_field('patient_surgeon',$pid);
					//$patient_img_url= get_the_post_thumbnail_url($pid,'full');
					$patient_name = $fname;
				  if($mname){
					$patient_name .= ' '.$mname;
				  }
				  if($lname){
					$patient_name .= ' '.$lname;
				  }	

				$gender= get_field('patient_gender',$pid);
				$email= get_field('patient_email',$pid);
				
				$address= get_field('patient_haddress',$pid);
				$street= get_field('patient_hstreet',$pid);
				$suburb= get_field('patient__hsuburb',$pid);
				$city= get_field('patient__hcity',$pid);
				$postcode= get_field('patient__hpostcode',$pid);

				if($street){
					$address .= ' '.$street;
				}
				if($suburb){
					$address .= ' '.$suburb;
				}
				if($city){
					$address .= ' '.$city;
				}
				if($postcode){
					$address .= ' '.$postcode;
				}
	
				$speciality= get_field('patient_speciality',$pid);
				$operation_title= get_field('patient_operation_title',$pid);
				$time= get_field('patient_time',$pid);
				$stages= get_field('patient_stages',$pid);
				$actions= get_field('patient_actions',$pid);
				$change_over= get_field('patient_change_over',$pid);		
				$age= get_field('patient_age',$pid);	
				$operation_number= get_field('patient_operation_number',$pid);	
				$patient_consent_form = get_field('patient_upload_consent_form',$pid);
				$patient_documents = get_field('patient_documents',$pid);
				$hospital_name = get_field('patient_hospital_name',$pid);
				$patient_img_url= get_site_url().'/wp-content/uploads/2019/05/user-female.png';
			  if($gender == 'male'){
				$patient_img_url= get_site_url().'/wp-content/uploads/2019/05/user_male.png';
			  }
			?>

				<article <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">
					<div class="gp-post-thumbnail gp-entry-featured">
						<img width="82" height="82" src="<?php echo $patient_img_url;?>" class="attachment-gp_featured_image size-gp_featured_image wp-post-image" alt="<?php echo get_the_title();?>" sizes="(max-width: 82px) 100vw, 82px">
					</div>
						<?php /*if ( has_post_thumbnail() && $display_image == 'enabled'){ ?>

							<div class="gp-post-thumbnail gp-entry-featured">
								<?php the_post_thumbnail( 'gp_featured_image' ); ?>
								<?php $attachment_id = get_post( get_post_thumbnail_id() ); if ( $attachment_id->post_excerpt ) { ?><div class="wp-caption-text"><?php echo esc_attr( $attachment_id->post_excerpt ); ?></div><?php } ?>
							</div>

						<?php }*/ ?>
						<div class="mainprofilecontent">
							<div class="profletitle">
									<h2 class="gp-loop-title"><?php echo $patient_name.' '.get_the_title();?>	</h2>
										<div class="gp-loop-meta">
						 <span class="gp-post-meta gp-meta-date"><?php echo $date_procedure;?></span>
						<?php if(is_user_logged_in() && (get_current_user_id() == $auth_id)){?> 
						 <a href="<?php echo get_site_url();?>/operation-case/edit-patient/?pid=<?php echo $pid;?>&post_edit=1"><i class="fa fa-cog" aria-hidden="true"></i> Edit</a>
						<?php } ?>
						 <i class="fa fa-hospital-o" aria-hidden="true"></i> <?php echo $hospital_name;?>						
						</div>

					</div>
					<div class="gp-entry-content" itemprop="text">
						Time: <?php echo $time;?><br>
						Stages: <?php echo $stages;?><br>
						Actions: <?php echo $actions;?><br>
						Change over: <?php echo $change_over;?><br>
						<br><br>
						<?php echo ucfirst($gender);?>, Age <?php echo $age;?><br>
						<?php echo $email;?><br>
						<?php echo address;?><br><br>
						Surgeon: <?php echo $surgeon;?><br>
						Specialty: <?php echo $speciality;?><br>
						Operation Number: <?php echo $operation_number;?><br><br>
						<b>Notes</b><br>
							<?php the_content(); ?>
						<hr>
						<b>Patient Consent</b><br>
						<?php 
						 if(is_user_logged_in()) { 
						  $options = array(
									'post_id' => $pid,
									'field_groups' => array(100000325), 							 
									'form' => true,													
									'return' => add_query_arg('updated', 'true', get_permalink()),
									'html_before_fields' => '<div class="form-patient_consent">',
                                    'html_after_fields' => '</div>',
									'updated_message' => __("Data updated.", 'acf'),
									'html_updated_message' => '<div class="alert alert-success" role="alert">%s</div>',
									//'submit_value' => 'Update' 
								);	 								
					        acf_form($options);
						} else {
						 if(sizeof($patient_consent_form) > 0){		
						?>
						<div class="acf-file-uploader sknew">
						<div class="show-if-value file-wrap">
						<div class="file-icon">
						 <img data-name="icon" src="<?php echo $patient_consent_form['icon'];?>" alt="">
						</div>
						<div class="file-info">
						 <p>
						  <a href="<?php echo $patient_consent_form['url'];?>" target="_blank"><?php echo $patient_consent_form['title'];?></a>
						  </p>
						</div>
						</div>
					</div>
						<?php }} ?>
						
					</div>
				</div>
					
					<?php /*if ( ! function_exists( 'pmpro_has_membership_access' ) OR ( function_exists( 'pmpro_has_membership_access' ) && pmpro_has_membership_access() ) ) { ?>					
						
					<?php }*/ ?>	

				</article>
				
			</div>

			<?php //get_sidebar( 'left' ); ?>

		<aside id="gp-sidebar-right" class="gp-sidebar" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
		 <div class="video-section">
		  <h3>Videos  
		  <?php if(is_user_logged_in() && (get_current_user_id() == $auth_id)){?> 
		  <a class="admedia" href="<?php echo get_site_url();?>/operation-case/add-patient-video/?id=<?php echo $pid;?>"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Files</a>
		  <?php } ?>
		  
		  </h3>
		<?php 
			$video_args = array(			 
						'author'        => $auth_id,
						'post_status' 	=> 'publish',
						'post_type'     => 'patient_video',
						'orderby'       => 'ID',
						'order'         => 'ASC',
						'posts_per_page'=> 4,
						'meta_query'=> array(   
                                array(              
                                    'key'    => 'patient_id',
                                    'value'  => $pid,
                                    'compare'=> '='    
                                )                               
                               )
					);
			$vd_query = new WP_Query($video_args);			
		?>	
		<?php if($vd_query->have_posts()): ?>		
		<div class="videowrap clearfix">
		<?php while($vd_query->have_posts()): $vd_query->the_post(); ?>
		<?php 
			$vid = get_the_ID(); 
			$video_url= get_field('patient_video_url',$vid);
			$lastmodified = get_the_modified_time('U');
			$posted = get_the_time('U');			
			$terms = get_the_terms($vid, 'video_category');
			$cats = null;     
			foreach($terms as $term){
				$cats .= $term->name.', ';
			}
		?>			
				<div class="videoggrid">				 
	<div class="videogridcuatom-inner">
			<div class="mpp-item-content mpp-video-content mpp-video-player">
				<iframe width="350" height="197" src="<?php echo $video_url;?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>					
			</div>
      <div class="cid_info">
		<h3 class="vid-title"><a href="<?php echo get_permalink();?>"><?php echo get_the_title();?></a></h3>
		<p class="vid-meta"> Posted <?php echo human_time_diff($posted,current_time('U'));?> ago by <a href="<?php echo get_site_url().'/members/'.get_the_author_meta('user_nicename');?>"><?php echo get_the_author_meta('display_name');?></a></p>
		
		<div class="mpp-item-description mpp-video-description">
		 <?php echo wp_trim_words( get_the_content(), 25, '[...]' );?>
		</div>	 

		<div class="mpp-media-meta-bottom">
			<span class="cate"><i class="fa fa-folder"></i> <?php echo rtrim($cats,", ");?></span>
			<span class="view"><i class="fa fa-eye"></i> <?php echo spvc_get_viewcount($vid);?> View</span>
		</div>
			</div></div>  
				</div>
	    <?php endwhile; ?>
			</div>
		<?php endif; wp_reset_postdata(); ?>

		 
		 </div>
		 <hr>
		 <div class="document-section">
		 <h3>Documents</h3>
		 <?php 
				if(is_user_logged_in()) { 
					$options = array(
									'post_id' => $pid,
									'field_groups' => array(100000312), 							 
									'form' => true,													
									'return' => add_query_arg('updated', 'true', get_permalink()),
									'html_before_fields' => '<div class="form-document">',
                                    'html_after_fields' => '</div>',
									'updated_message' => __("Data updated.", 'acf'),
									'html_updated_message' => '<div class="alert alert-success" role="alert">%s</div>',
									//'submit_value' => 'Update' 
								);	 								
					        acf_form($options);
				} else {
					
					if(sizeof($patient_documents) > 0){
						foreach($patient_documents as $pd){
						  $document = $pd['patient_document'];
			?>
					<div class="acf-file-uploader sknew">
					<div class="show-if-value file-wrap clearfix" id="<?php echo $document['ID'];?>">
						<div class="file-icon">
						 <img data-name="icon" src="<?php echo $document['icon'];?>" alt="">
						</div>
						<div class="file-info">
						 <p>
						  <a href="<?php echo $document['url'];?>" target="_blank"><?php echo $document['title'];?></a>
						  </p>
						</div>
					</div>
					</div>
			<?php }}} ?>
			</div>			
		</aside>
		</div>

		<?php do_action( 'ghostpool_end_content_wrapper' ); ?>		
		<div class="gp-clear"></div>
	</div>

<?php endwhile; endif; ?>
<?php get_footer(); ?>